(ns clojure.jdbc.enum
  (:require [clojure.java.jdbc :as jdbc]
            [clojure.string :as str]))

(defmulti get-enums first)

(defmethod get-enums :default [_]
  nil)

(defmethod get-enums "postgresql" [[_ db-spec]]
  (let [query "SELECT n.nspname AS schema, t.typname AS name, 
                  array_to_string(array_agg(e.enumlabel ORDER BY e.enumsortorder), ',') AS values
               FROM pg_type t
               JOIN pg_enum e ON t.oid = e.enumtypid
               JOIN pg_catalog.pg_namespace n ON n.oid = t.typnamespace
               GROUP BY schema,name"
        split-values #(str/split % #",")]
    (->> (jdbc/query db-spec [query])
         (map #(update % :values split-values))
         vec)))