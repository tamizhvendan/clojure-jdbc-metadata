(ns clojure.jdbc.metadata
  (:require [clojure.java.jdbc :as jdbc]
            [clojure.set :as set]
            [clojure.string :as str]
            [clojure.jdbc.enum :as enum]))

(defn- map-kv [f m]
  (reduce-kv (fn [state k v] (assoc state k (f v))) {} m))

(defn- relation-schema-and-name [{:keys [schema table-name]}]
  [schema table-name])

(defn- transform-table-type [table-type]
  (case table-type
    "TABLE" :table
    "VIEW" :view))

(defn- transform-table-metadata [{:keys [table_schem table_name remarks table_type]}]
  {:schema table_schem
   :table-name table_name
   :remarks remarks
   :type (transform-table-type table_type)})

(defn- get-relations [metadata schema-pattern]
  (->> (into-array String ["TABLE" "VIEW"])
       (.getTables metadata nil schema-pattern nil)
       jdbc/metadata-result
       (map transform-table-metadata)
       (group-by relation-schema-and-name)
       (map-kv first)))

(defn- transform-pk-metadata [{:keys [table_schem table_name pk_name key_seq column_name]}]
  {:schema table_schem
   :table-name table_name
   :primary-key-name pk_name
   :column-order key_seq
   :column-name column_name})

(defn- reduce-pk-metadata [x]
  (let [pk-name (:primary-key-name (first x))
        columns (map (fn [{:keys [column-name column-order]}]
                       {:name column-name :order column-order}) x)]
    {:primary-key {:name pk-name
                   :columns (vec columns)}}))

(defn- get-primary-key [metadata schema-pattern]
  (->> (.getPrimaryKeys metadata nil schema-pattern nil)
       jdbc/metadata-result
       (map transform-pk-metadata)
       (group-by relation-schema-and-name)
       (map-kv reduce-pk-metadata)))

(defn- transform-fk-metadata [{:keys [fktable_name fktable_schem fkcolumn_name
                                      pktable_name pktable_schem pkcolumn_name]}]
  {:schema fktable_schem
   :column-name fkcolumn_name
   :table-name fktable_name
   :ref-table-name pktable_name
   :ref-table-schema pktable_schem
   :ref-column-name pkcolumn_name})

(defn- fk-metadata->ref-metadata [{:keys [column-name ref-table-name ref-table-schema ref-column-name]}]
  {:column-name column-name
   :reference {:table {:name ref-table-name
                       :schema ref-table-schema}
               :column-name ref-column-name}})

(defn- reduce-fk-metadata [x]
  {:foreign-keys (vec (map fk-metadata->ref-metadata x))})

(defn- get-foreign-keys [metadata schema-pattern]
  (->> (.getImportedKeys metadata nil schema-pattern nil)
       jdbc/metadata-result
       (map transform-fk-metadata)
       (group-by relation-schema-and-name)
       (map-kv reduce-fk-metadata)))

(defn- yes-no->boolean [yes-or-no]
  (case yes-or-no
    "YES" true
    "NO" false))

(defn- transform-col-metadata [col-metadata]
  (let [col-name-mapping {:is_autoincrement :is-auto-increment
                          :is_nullable :is-nullable
                          :data_type :jdbc-data-type
                          :type_name :data-type
                          :column_def :default
                          :column_size :column-size
                          :ordinal_position :ordinal-position
                          :column_name :name
                          :table_schem :schema
                          :table_name :table-name
                          :remarks :remarks}]
    (-> (set/rename-keys col-metadata col-name-mapping)
        (select-keys (vals col-name-mapping))
        (update :is-auto-increment yes-no->boolean)
        (update :is-nullable yes-no->boolean))))

(defn- reduce-col-metadata [x]
  {:columns (vec (map #(dissoc % :schema :table-name) x))})

(defn- get-columns [metadata schema-pattern]
  (->> (.getColumns metadata nil schema-pattern "%" nil)
       jdbc/metadata-result
       (map transform-col-metadata)
       (group-by relation-schema-and-name)
       (map-kv reduce-col-metadata)))

(defn- db-product-name [db-spec]
  (jdbc/with-db-metadata [md db-spec]
    (-> (.getDatabaseProductName md)
        str/lower-case)))

(defn fetch [db-spec]
  (jdbc/with-db-metadata [metadata db-spec]
    (let [relations (get-relations metadata "%")
          primary-key (get-primary-key metadata "")
          foreign-keys (get-foreign-keys metadata "")
          columns (get-columns metadata "%")
          db-product-name (db-product-name db-spec)
          enums (enum/get-enums [db-product-name db-spec])]
      (->> (merge-with merge relations primary-key foreign-keys columns)
           vals
           (filter #(some? (:type %)))
           vec
           (hash-map :enums enums :db {:product-name db-product-name} :relations)))))